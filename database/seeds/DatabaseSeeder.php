<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 5)->create();
        factory(App\ForumGroup::class, 3)->create();
        factory(App\Forum::class, 10)->create();
        factory(App\Topic::class, 20)->create();
        factory(App\Comment::class, 25)->create();
    }
}
