<?php

use App\User;
use Bican\Roles\Models\Role;
use Illuminate\Database\Migrations\Migration;

class CreateAdminUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create admin account by default.
        $user = User::create(['name'=>'Admin', 'email'=>'admin@forum.com', 'password'=>bcrypt('admin')]);
        $adminRole = Role::create([
            'name' => 'Admin',
            'slug' => 'admin',
        ]);
        $user->attachRole($adminRole);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
