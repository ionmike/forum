<?php

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\ForumGroup::class, function(Faker\Generator $faker) {
    return [
        'name' => $faker->sentence(1),
    ];
});

$factory->define(App\Forum::class, function(Faker\Generator $faker) {
    return [
        'name' => $faker->sentence(1),
        'forum_group_id' => $faker->numberBetween(1, 3),
    ];
});

$factory->define(App\Topic::class, function(Faker\Generator $faker) {
    return [
        'title' => $faker->sentence(3),
        'forum_id' => $faker->numberBetween(1, 10),
        'user_id' => $faker->numberBetween(1, 5),
        'text' => $faker->text(),
        'last_activity' => \Carbon\Carbon::now(),
    ];
});

$factory->define(App\Comment::class, function(Faker\Generator $faker) {
    return [
        'topic_id' => $faker->numberBetween(1, 20),
        'user_id' => $faker->numberBetween(1, 6),
        'text' => $faker->text(100),
    ];
});
