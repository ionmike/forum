$(document).ready(function() {
    /*
    * Focus on modal show
    * */
    $('#forumModal').on('shown.bs.modal', function () {
        $('#forum-name').focus();
    });

    $('#forumGroupModal').on('shown.bs.modal', function () {
        $('#forum-group-name').focus();
    });

    /*
    * Forum creation section
    * */
    // Display modal form for creating new forum.
    $('.open-forum-modal').click(function(){
        $('#frmForum').trigger("reset");
        $('#forum-form-errors').empty();
        $('#forum-group-id').val($(this).val());
        $('#forumModal').modal('show');
    });

    // Click on create new forum in modal window.
    $('#btn-create').click(function (e) {
        e.preventDefault();
        var formData = {
            name: $('#forum-name').val(),
            forum_group_id: $('#forum-group-id').val()
        };
        $.ajax({
            type: "POST",
            url: base_url + 'forum',
            data: formData,
            dataType: 'json',
            success: function (data) {
                var forum = '<tr> <td colspan="7"> <a href="forum/' + data.id + '">' + data.name + '</a> </td></tr>';
                $('#forums-table-'+data.forum_group_id).append(forum);
                toastr.success(trans('forum.created'));
                $('#frmForum').trigger("reset");
                $('#forumModal').modal('hide');
            },
            error: function (data) {
                var errorsHtml = '<div class="alert alert-danger"><ul>';
                if(data.status == 422) {
                    //noinspection JSUnresolvedVariable
                    $.each(data.responseJSON, function(key, value ) {
                        errorsHtml += '<li>' + value[0] + '</li>';
                    });
                    errorsHtml += '</ul></di>';
                    $('#forum-form-errors').html(errorsHtml);
                }
                console.log('Error:', data);
            }
        });
    });

    /*
    * Forum group section
    * */
    // Display modal form for creating new forum group.
    $('#btn-create-forum-group').click(function(){
        $('#btn-forum-group').val("add").text(trans('global.create'));
        $('#frmForumGroup').trigger("reset");
        $('#forumGroupModalLabel').text(trans('forumgroup.addmodal'));
        $('#forum-group-form-errors').empty();
        $('#forumGroupModal').modal('show');
    });

    // Display modal form for forum group editing.
    $('.open-forum-group-modal').click(function(){
        var forum_group_id = $(this).val();
        $.get(base_url + 'forum/group/' + forum_group_id + '/edit', function (data) {
            $('#forum-group-id').val(data.id);
            $('#forum-group-name').val(data.name);
            $('#btn-forum-group').val("update").text(trans('global.save'));
            $('#forumGroupModalLabel').text(trans('forumgroup.editmodal'));
            $('#forum-group-form-errors').empty();
            $('#forumGroupModal').modal('show');
        })
    });

    // Actions for forum group creation/editing.
    $('#btn-forum-group').click(function (e) {
        e.preventDefault();

        var formData = {
            name: $('#forum-group-name').val()
        };

        // Used to determine the http verb to use [add=POST], [update=PUT]
        var state = $('#btn-forum-group').val();
        var forum_group_id = $('#forum-group-id').val();

        var type, action_url;
        if (state == "add") {
            type = "POST";
            action_url = base_url + "forum/group";
        }
        if (state == "update") {
            type = "PATCH";
            action_url = base_url + "forum/group/" + forum_group_id + "/update";
        }

        $.ajax({
            type: type,
            url: action_url,
            data: formData,
            dataType: 'json',
            success: function (data) {
                if (state == "add") {
                    // We don't make tons of work, just update page
                    location.reload();
                }
                if (state == "update") {
                    toastr.success(trans('forumgroup.updated'));
                    $("#forum-group-name-text-" + forum_group_id).replaceWith(data.name);
                }
                $('#forumGroupModal').modal('hide')
            },
            error: function (data) {
                var errorsHtml = '<div class="alert alert-danger"><ul>';
                if(data.status == 422) {
                    //noinspection JSUnresolvedVariable
                    $.each(data.responseJSON, function(key, value ) {
                        errorsHtml += '<li>' + value[0] + '</li>';
                    });
                    errorsHtml += '</ul></di>';
                    $('#forum-group-form-errors').html(errorsHtml);
                }
                console.log('Error:', data);
            }
        });
    });

    // Delete forum group (button action)
    $('.delete-forum-group').click(function(){
        var forum_group_id = $(this).val();
        if(confirm(trans('global.sure'))) {
            $.ajax({
                type: "DELETE",
                url: base_url + 'forum/group/' + forum_group_id + '/destroy',
                success: function () {
                    location.reload();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
    });

    /*
    * Forum control panel togler.
    * */
    $('#toggle-forum-cp').click(function(){
        $('.forum-groups-control-buttons').fadeToggle(200);
    });

    // Spoilers
    $('.spoiler').click(function(){
        $(this).next('.spoiler-content').slideToggle();
    });
});