$(document).ready(function(){
    // On modal show focus on input field.
    $('#forumModal').on('shown.bs.modal', function () {
        $('#forum-name').focus();
    });

    // delete forum and redirect to main page
    $('.delete-forum').click(function(){
        var forum_id = $(this).val();
        if(confirm(trans('global.sure'))) {
            $.ajax({
                type: "DELETE",
                url: base_url + 'forum/' + forum_id + '/destroy',
                success: function () {
                    window.location.href = '../';
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
    });

    // Display modal form for forum editing.
    $('.open-modal').click(function(){
        var forum_id = $(this).val();
        $.get(base_url + 'forum/' + forum_id + '/edit', function (data) {
            $('#forum_id').val(data.id);
            $('#forum-name').val(data.name);
            $('#form-errors').empty();
            $('#forumModal').modal('show');
        })
    });

    // Update forum name.
    $("#btn-save").click(function (e) {
        e.preventDefault();
        var formData = {
            name: $('#forum-name').val()
        };
        var forum_id = $('#forum_id').val();

        $.ajax({
            type: "PATCH",
            url: base_url + 'forum/' + forum_id + '/update',
            data: formData,
            dataType: 'json',
            success: function (data) {
                toastr.success(trans('forum.updated'))
                $("#forum-name-text").text(data.name);
                $('#frmForum').trigger("reset");
                $('#forumModal').modal('hide');
            },
            error: function (data) {
                var errorsHtml = '<div class="alert alert-danger"><ul>';
                if(data.status == 422) {
                    $.each(data.responseJSON, function(key, value ) {
                        errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                    });
                    errorsHtml += '</ul></di>';
                    $('#form-errors').html(errorsHtml);
                }
                console.log('Error:', data);
            }
        });
    });
});