$(document).ready(function() {
    tinymce.init({
        selector: '.editor',
        plugins: ['advlist lists link table textcolor colorpicker code'],
        toolbar: 'undo redo | bold italic underline code | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | forecolor backcolor | link table',
        menubar: false,
        skin: "light",
        max_height: 500,
        min_height: 100
    });
});