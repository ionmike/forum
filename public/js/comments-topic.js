$(document).ready(function(){
    $('#commentModal').on('shown.bs.modal', function () {
        $('#comment-text').focus();
    });

    // Delete comment and remove it from list.
    $('.delete-comment').click(function(){
        var comment_id = $(this).val();
        if(confirm(trans('global.sure'))) {
            $.ajax({
                type: "DELETE",
                url: base_url + 'comment/' + comment_id + '/destroy',
                success: function () {
                    toastr.warning(trans('comments.deleted'));
                    $("#comment-" + comment_id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
    });

    // Display modal form for comment editing.
    $('.open-modal').click(function(){
        var comment_id = $(this).val();
        $.get(base_url + 'comment/' + comment_id + '/edit', function (data) {
            $('#comment_id').val(data.id);
            $('#comment-text').val(data.text);
            $('#form-errors').empty();
            $('#commentModal').modal('show');
        })
    });

    // Update existing comment.
    $("#btn-save").click(function (e) {
        e.preventDefault();
        var formData = {
            text: $('#comment-text').val()
        };
        var comment_id = $('#comment_id').val();

        $.ajax({
            type: "PATCH",
            url: base_url + 'comment/' + comment_id + '/update',
            data: formData,
            dataType: 'json',
            success: function (data) {
                $("#comment-text-" + comment_id).text(data.text);
                toastr.info(trans('comments.updated'));
                $('#frmComment').trigger("reset");
                $('#commentModal').modal('hide');
            },
            error: function (data) {
                var errorsHtml = '<div class="alert alert-danger"><ul>';
                if(data.status == 422) {
                    //noinspection JSUnresolvedVariable
                    $.each(data.responseJSON, function(key, value ) {
                        errorsHtml += '<li>' + value[0] + '</li>';
                    });
                    errorsHtml += '</ul></di>';
                    $('#form-errors').html(errorsHtml);
                }
                console.log('Error:', data);
            }
        });
    });

    $(".delete-topic").on("click", function(){
        return confirm(trans('global.sure'));
    });
});