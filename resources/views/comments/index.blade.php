<div class="container">
    @forelse($comments as $comment)
        <div id="comment-{{ $comment->id }}">
            <div name="comment-{{ $comment->id }}" class="well well-sm" style="word-wrap: break-word;">
                <img src="{{ asset('images/uploads/user_avatars/'.$comment->user->avatar) }}" style="border-radius: 5px; border: 1px solid #e3e3e3; height: 25px; width: 25px;">
                <a href="{{ url('user/'.$comment->user->id) }}" style="@if($comment->user->hasRole('admin'))color: #dc353b; @endif line-height: 35px;">
                    {{ $comment->user->name }}
                </a>
                <div id="comment-text-{{ $comment->id }}" style="margin: 0.5em 0;">
                    {{ $comment->text }}
                </div>
                <div class="row">
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 pull-left">
                        <small>
                            <a href="#comment-{{$comment->id}}">{{ $comment->created_at }}</a>
                            @if($comment->isEdited())<br>{{ trans('comments.changed_at') }} {{ LocalizedCarbon::instance($comment->updated_at)->diffForHumans() }}@endif
                        </small>
                    </div>
                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 pull-right">
                        @if(Auth::check() && (Auth::user()->id == $comment->user_id || Auth::user()->hasRole('admin')))
                            <div class="btn-group pull-right">
                                <button class="btn btn-link btn-sm open-modal" value="{{$comment->id}}">
                                    <i class="fa fa-pencil"></i> {{ trans('comments.edit') }}
                                </button>
                                <button class="btn btn-link btn-sm delete-comment" value="{{$comment->id}}">
                                    <i class="fa fa-trash"></i> {{ trans('comments.delete') }}
                                </button>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @empty
        <div class="panel panel-info">
              <div class="panel-heading">
                  {{ trans('comments.nocomments') }}
              </div>
        </div>
    @endforelse
</div>
<div class="text-center">{{ $comments->links() }}</div>