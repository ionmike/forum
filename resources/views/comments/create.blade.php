<div class="container">
    <div class="well">
        {!! Form::open(['url'=>LaravelLocalization::localizeURL('comment/create')]) !!}
            @include('partials.comment_form', ['submitButtonText' => trans('comments.create')])
            {{ Form::hidden('topic_id', $topic->id) }}
        {!! Form::close() !!}
    </div>
</div>