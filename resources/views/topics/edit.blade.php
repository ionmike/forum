@extends('layouts.app')

@section('title'){{ trans('topic.edit') }}
@endsection

@section('content')
    <div class="container">
        {{ Form::model($topic, ['method'=> 'PATCH', 'url' => LaravelLocalization::localizeURL('topic/'.$topic->id)]) }}
            @include('partials.topic_form', ['submitButtonText'=>trans('topic.edit')])
        {!! Form::close() !!}
    </div>
@endsection

@section('page-scripts')
    <script src="{{ asset('js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('js/topic-editor.js') }}"></script>
@endsection