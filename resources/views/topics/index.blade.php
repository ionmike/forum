@extends('layouts.app')

@section('title'){{ $topic->title }} | {{ $appSettingsForumName }}
@endsection

@section('content')
<div class="container">
    <a href="{{ url('forum/'.$topic->forum_id) }}" class="btn btn-default btn-sm">{{ trans('topic.backtoforum') }}</a>
</div>
<br>
<div class="container">
    <div class="panel panel-default">
        <div class="panel-body">
            @if($topic->isAttached())<span class="label label-info">{{ trans('topic.attached') }}</span> <br>@endif
            <h3 style="margin: 5px 0;">
                {{ $topic->title }}
            </h3>
            <br>
            {!! $topic->text !!}
        </div>
        <div class="panel-footer" style="overflow: hidden;">
            <div class="pull-left">
                <img src="{{ asset('images/uploads/user_avatars/'.$topic->user->avatar) }}"
                     style="border-radius: 5px; border: 1px solid #e3e3e3; height: 25px; width: 25px;" >
                <a href="{{ url('user/'.$topic->user->id) }}" style="@if($topic->user->hasRole('admin'))color: #dc353b; @endif">{{ $topic->user->name }}</a>
                <br>
                <small>{{ $topic->created_at }}</small>
            </div>
            @if(Auth::check() && (Auth::user()->id == $topic->user_id || Auth::user()->hasRole('admin')))
                <div class="btn-group btn-group-sm pull-right" style="padding-top: 6px;">
                    <a href="{{ url('topic/'.$topic->id.'/edit') }}" class="btn btn-primary"><i class="fa fa-pencil"></i> {{ trans('topic.edit') }}</a>
                    <a href="{{ url('topic/'.$topic->id.'/destroy') }}" class="btn btn-danger delete-topic"><i class="fa fa-trash"></i> {{ trans('topic.delete') }}</a>
                </div>
            @endif
        </div>
    </div>
</div>

<br>

@include('comments.index')

@if(Auth::check())
    @include('comments.create')
@endif

<div class="modal fade" id="commentModal" tabindex="-1" role="dialog" aria-labelledby="commentModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="commentModalLabel">{{ trans('comments.edit') }}</h4>
            </div>
            <div class="modal-body">
                <form id="frmComment" name="frmComment" novalidate onsubmit="return false;">
                    <div class="form-group">
                        <textarea type="text" class="form-control" id="comment-text" name="text" placeholder="{{ trans('comments.modalplaceholder') }}"></textarea>
                    </div>
                </form>
                <div id="form-errors"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn-save">{{ trans('global.save') }}</button>
                <input type="hidden" id="comment_id" name="comment_id" value="0">
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-scripts')
    <script src="{{ asset('js/comments-topic.js') }}"></script>
@endsection