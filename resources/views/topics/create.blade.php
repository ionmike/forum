@extends('layouts.app')

@section('title'){{ trans('topic.create') }}
@endsection

@section('content')
    <div class="container">
        {!! Form::open(['url'=>LaravelLocalization::localizeURL('topic/create')]) !!}
            @include('partials.topic_form', ['submitButtonText'=>trans('topic.create')])
            {{-- NOTE: ionmike user can manipulate forum_id. but he's doing nothing special. --}}
            {{ Form::hidden('forum_id', $forum_id) }}
        {!! Form::close() !!}
    </div>
@endsection

@section('page-scripts')
    <script src="{{ asset('js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('js/topic-editor.js') }}"></script>
@endsection