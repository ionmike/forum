@extends('layouts.app')

@section('title'){{ $user->name }} | {{ $appSettingsForumName }}
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="well col-xs-8 col-sm-8 col-md-8 col-lg-8 col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
            <img src="{{ asset('images/uploads/user_avatars/'.$user->avatar) }}"
                 style="border-radius: 5px; border: 1px solid #e3e3e3;"
                 class="center-block">
            <h2 class="text-center" style="@if($user->hasRole('admin'))color: #dc353b; @endif">{{ $user->name }}</h2>
            <p class="text-center">{{ trans('profile.registeredat') }}: {{ LocalizedCarbon::instance($user->created_at)->diffForHumans() }}</p>
            <div class="row">
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xs-offset-4 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 text-center">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <i class="fa fa-newspaper-o"></i> {{ $user->topics()->count() }}
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <i class="fa fa-comments"></i> {{ $user->comments()->count() }}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    @if(Auth::user()->id == $user->id || Auth::user()->hasRole('admin'))
                        <div class="pull-right">
                            <a href="{{ LaravelLocalization::localizeURL('user/'.$user->id.'/edit') }}" class="btn btn-default">
                                <i class="fa fa-cog"></i> {{ trans('profile.settings') }}
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <br>
    @unless($userLastFiveTopics->isEmpty())
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">{{ trans('profile.lastfivetopics') }}</h3>
            </div>
            <table class="panel-body table table-hover">
                <tbody>
                @foreach($userLastFiveTopics as $topic)
                    <tr>
                        <td><a href="{{ url('topic/'.$topic->id) }}">{{ $topic->title }}</a></td>
                        <td class="col-xs-3 col-sm-2 col-md-2 col-lg-2">{{ LocalizedCarbon::instance($topic->created_at)->diffForHumans() }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endunless
    @unless($userLastFiveComments->isEmpty())
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">{{ trans('profile.lastfivecomments') }}</h3>
            </div>
            <table class="panel-body table table-hover">
                <tbody>
                    @foreach($userLastFiveComments as $comment)
                        <tr>
                            <td>{{ str_limit($comment->text, 115, '...') }}</td>
                            <td class="col-xs-3 col-sm-2 col-md-2 col-lg-2">
                                <a href="{{ url('topic/'.$comment->topic->id.$comment->topic->getCommentPage($comment->id).'#comment-'.$comment->id) }}">
                                    {{ LocalizedCarbon::instance($comment->updated_at)->diffForHumans() }}
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endunless
</div>
@endsection