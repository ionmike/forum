@extends('layouts.app')

@section('title'){{ trans('profile.settings') }}
@endsection

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">{{ trans('profile.settings') }}</div>
        <div class="panel-body">
            {{ Form::model($user, ['method'=> 'PATCH', 'url' => LaravelLocalization::localizeURL('user/'.$user->id), 'files' => true ]) }}
                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    {{ Form::label('name', trans('register.name')) }}
                    {{ Form::text('name', null, ['class'=>'form-control']) }}
                    @if($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    {{ Form::label('email', trans('register.email')) }}
                    {{ Form::text('email', null, ['class'=>'form-control']) }}
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div id="password-fields" style="{{ $errors->has('password') ? '' : 'display: none;' }}">
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        {{ Form::label('password', trans('register.password')) }}
                        {{ Form::password('password', ['class'=>'form-control']) }}
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        {{ Form::label('password_confirmation', trans('register.confirm')) }}
                        {{ Form::password('password_confirmation', ['class'=>'form-control']) }}
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                @role('admin')
                    <div class="form-group">
                        {{ Form::label('roles_list', trans('profile.roles')) }}
                        {{ Form::select('roles_list[]', $roles, null, ['class'=>'form-control roles-select', 'multiple']) }}
                    </div>
                @endrole
                <div class="form-group {{ $errors->has('profile_image') ? ' has-error' : '' }}">
                    {{ Form::label('profile_image', trans('profile.avatar')) }}
                    {{ Form::file('profile_image') }}
                    @if ($errors->has('profile_image'))
                        <span class="help-block">
                            <strong>{{ $errors->first('profile_image') }}</strong>
                        </span>
                    @endif
                </div>
                {{ Form::submit(trans('global.save') ,['class'=>'btn btn-primary']) }}
                @unless($errors->has('password'))
                    <a class="btn btn-warning pull-right" id="change-password"><i class="fa fa-warning"></i> {{ trans('profile.changepassword') }}</a>
                @endunless
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection

@section('page-scripts')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script type="text/javascript">
        $('.roles-select').select2();

        $('#change-password').click(function() {
            $('#change-password').fadeOut(function() {
                $('#password-fields').slideDown();
            });
        });
    </script>
@endsection