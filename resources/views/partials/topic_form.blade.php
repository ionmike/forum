<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
    {{ Form::label('title', trans('topic.title')) }}
    {{ Form::text('title', null, ['class'=>'form-control']) }}
    @if($errors->has('title'))
        <span class="help-block">
            <strong>{{ $errors->first('title') }}</strong>
        </span>
    @endif
</div>
<div class="form-group">
    {{ Form::textarea('text', null, ['class'=>'form-control editor']) }}
    @if($errors->has('text'))
        <span class="help-block">
            <strong>{{ $errors->first('text') }}</strong>
        </span>
    @endif
</div>
@role('admin')
    <div class="form-group">
        <label>{{ Form::checkbox('attached') }} {{ trans('topic.attached') }}</label>
    </div>
@endrole
<div class="form-group">
    {!! Form::submit($submitButtonText, ['class'=> 'btn btn-primary form-control']) !!}
</div>
