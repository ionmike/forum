<div class="form-group {{ $errors->has('text') ? 'has-error' : '' }}">
    {{ Form::textarea('text', null, ['class'=>'form-control', 'placeholder' => trans('comments.writeplaceholder')]) }}
    @if($errors->has('text'))
        <span class="help-block">
            <strong>{{ $errors->first('text') }}</strong>
        </span>
    @endif
</div>
<div class="form-group">
    {!! Form::submit($submitButtonText, ['class'=> 'btn btn-primary form-control']) !!}
</div>
