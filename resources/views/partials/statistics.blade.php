<div class="container">
    @unless($lastTenTopics->isEmpty())
        <div class="panel panel-default">
            <div class="panel-heading spoiler">
                <h3 class="panel-title"><i class="fa fa-newspaper-o"></i>  {{ trans('index.lasttentopics') }}</h3>
            </div>
            <div class="spoiler-content">
                <table class="panel-body table table-hover" style="margin-bottom: 0;">
                    <tbody>
                        @foreach($lastTenTopics as $topic)
                            <tr class="row">
                                <td class="col-xs-4 col-sm-3 col-md-2 col-lg-2">
                                    <a href="{{ url('user/'.$topic->user->id) }}">{{ $topic->user->name }}</a>
                                </td>
                                <td><a href="{{url('topic/'.$topic->id)}}">{{$topic->title}}</a></td>
                                <td class="col-xs-4 col-sm-3 col-md-2 col-lg-2">
                                    {{ LocalizedCarbon::instance($topic->created_at)->diffForHumans() }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endunless
    @unless($lastTenComments->isEmpty())
        <div class="panel panel-default">
            <div class="panel-heading spoiler">
                <h3 class="panel-title"><i class="fa fa-comments"></i>  {{ trans('index.lasttencomments') }}</h3>
            </div>
            <div class="spoiler-content">
                <table class="panel-body table table-hover" style="margin-bottom: 0;">
                    <tbody>
                        @foreach($lastTenComments as $comment)
                            <tr class="row">
                                <td class="col-xs-4 col-sm-3 col-md-2 col-lg-2">
                                    <a href="{{ url('user/'.$comment->user->id) }}">{{ $comment->user->name }}</a>
                                </td>
                                <td>
                                    {{ str_limit($comment->text, 115, '...') }}
                                </td>
                                <td class="col-xs-4 col-sm-3 col-md-2 col-lg-2">
                                    <a href="{{ url('topic/'.$comment->topic->id.$comment->topic->getCommentPage($comment->id).'#comment-'.$comment->id) }}">
                                        {{ LocalizedCarbon::instance($comment->updated_at)->diffForHumans() }}
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endunless
</div>