@extends('layouts.app')

@section('title'){{ $appSettingsForumName }}
@endsection

@section('content')
<div class="container">
    @foreach($forumGroups as $forumGroup)
        <div id="forum-group-row-{{ $forumGroup->id }}">
            <div class="panel panel-default">
                <div class="panel-heading" style="overflow: hidden; line-height:30px;">
                    <span id="forum-group-name-text-{{ $forumGroup->id }}">{{ $forumGroup->name }}</span>
                    @role('admin')
                        <div class="btn-group pull-right forum-groups-control-buttons" style="display: none;">
                            <button type="button" class="btn btn-primary btn-sm open-forum-group-modal" value="{{ $forumGroup->id }}">
                                {{ trans('forumgroup.rename') }}
                            </button>
                            <button type="button" class="btn btn-danger btn-sm delete-forum-group" value="{{ $forumGroup->id }}">
                                {{ trans('forumgroup.delete') }}
                            </button>
                            <button type="button" class="btn btn-default btn-sm open-forum-modal" value="{{ $forumGroup->id }}">
                                <i class="fa fa-plus"></i> {{ trans('forum.create') }}
                            </button>
                        </div>
                    @endrole
                </div>
                <table class="panel-body table table-hover">
                    <tbody id="forums-table-{{$forumGroup->id}}">
                    @foreach($forumGroup->forums as $forum)
                        <tr>
                            @if(!$forum->topics->isEmpty())
                                <td><a href="{{ url('forum/'.$forum->id) }}"> {{ $forum->name }} </a></td>
                            @else
                                <td colspan="7"><a href="{{ url('forum/'.$forum->id) }}"> {{ $forum->name }} </a></td>
                            @endif
                            @if(!$forum->topics->isEmpty())
                                <td class="col-xs-3 col-sm-2 col-md-1 col-lg-1 text-center">
                                    {{ $forum->topics->count() }} {{trans_choice('index.topicnums', $forum->topics->count()) }}
                                </td>
                            @endif
                            @if($forum->lastActiveTopic())
                                <td class="col-xs-3 col-sm-4 col-md-3 col-lg-3">
                                    <a href="{{ url('topic/'.$forum->lastActiveTopic()->id) }}">{{ $forum->lastActiveTopic()->title }}</a>
                                </td>
                            @else
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endforeach

    @role('admin')
        <button class="btn" id="toggle-forum-cp"><i class="fa fa-circle"></i> {{ trans('index.admincp') }}</button>
        <button class="btn btn-primary forum-groups-control-buttons" style="display: none;" id="btn-create-forum-group">
            <i class="fa fa-plus"></i> {{ trans('forumgroup.create') }}
        </button>
    @endrole
</div>

@if(Auth::check())
<br>
    @include('partials.statistics')
@endif

<div class="modal fade" id="forumModal" tabindex="-1" role="dialog" aria-labelledby="forumModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="forumModalLabel">{{ trans('forum.create') }}</h4>
            </div>
            <div class="modal-body">
                <form id="frmForum" name="frmForum" novalidate onsubmit="return false;">
                    <div class="form-group">
                        <input type="text" class="form-control" id="forum-name" name="name" placeholder="{{ trans('forum.modalplaceholder') }}" autofocus>
                    </div>
                </form>
                <div id="forum-form-errors"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn-create">{{ trans('global.create') }}</button>
                <input type="hidden" id="forum-group-id" name="forum-group-id" value="0">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="forumGroupModal" tabindex="-1" role="dialog" aria-labelledby="forumGroupModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="forumGroupModalLabel"></h4>
            </div>
            <div class="modal-body">
                <form id="frmForumGroup" name="frmForumGroup" novalidate onsubmit="return false;">
                    <div class="form-group">
                        <input type="text" class="form-control" id="forum-group-name" name="name" placeholder="{{ trans('forumgroup.modalplaceholder') }}" autofocus>
                    </div>
                </form>
                <div id="forum-group-form-errors"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn-forum-group"></button>
                <input type="hidden" id="forum-group-id" name="forum-group-id" value="0">
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-scripts')
    <script src="{{ asset('js/forum-groups.js') }}"></script>
@endsection