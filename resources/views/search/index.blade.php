@extends('layouts.app')

@section('title'){{ trans('index.search') }}
@endsection

@section('content')
    <div class="container">
    	{!! Form::open(['method'=>'get', 'url'=> LaravelLocalization::localizeURL('/search')]) !!}
        <div class="input-group">
            {{ Form::text('search_text', null, ['class'=>'form-control', 'style'=>'width:70%;']) }}
            {{ Form::select('type', ['T' => trans('search.topic'), 'C' => trans('search.comment'), 'U' => trans('search.user')], 'T', ['class'=>'form-control', 'style'=>'width:30%;']) }}
            <span class="input-group-btn">
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            </span>
        </div>
        {!! Form::close() !!}
    </div>
    <br>
    <div class="container">
        @if(isset($results))
            @forelse($results as $result)
                <div class="well well-sm">
                    @if(get_class($result) == "App\Topic")
                        <span><a href="{{ url('topic/'.$result->id) }}">{{ $result->title }}</a></span>
                        <span class="pull-right">
                            <i class="fa fa-clock-o"></i> {{ LocalizedCarbon::instance($result->last_activity)->diffForHumans() }} |
                            <a href="{{ url('user/'.$result->user->id) }}">{{ $result->user->name }}</a>
                        </span>
                    @endif
                    @if(get_class($result) == "App\User")
                        <span><a href="{{ url('user/'.$result->id) }}">{{ $result->name }}</a></span>
                        <span class="pull-right">
                            <i class="fa fa-comments"></i> {{ $result->comments()->count() }} |
                            <i class="fa fa-newspaper-o"></i> {{ $result->topics()->count() }}
                        </span>
                    @endif
                    @if(get_class($result) == "App\Comment")
                        <a href="{{ url('user/'.$result->user->id) }}">{{ $result->user->name }}</a> <br>
                        {{ $result->text }} <br>
                        <a href="{{ url('topic/'.$result->topic->id.$result->topic->getCommentPage($result->id).'#comment-'.$result->id) }}">
                            <small><i class="fa fa-clock-o"></i> {{ LocalizedCarbon::instance($result->created_at)->diffForHumans() }}</small>
                        </a>
                    @endif
                </div>
            @empty
                <h3 class="text-center">{{ trans('search.noresults') }}</h3>
            @endforelse
            <div class="text-center">{!! $results->appends(['search_text' => $searchText, 'type'=> $searchType])->links() !!}</div>
        @else
            <h3 class="text-center">{{ trans('search.message') }}</h3>
        @endif
    </div>
@endsection