@extends('layouts.app')

@section('title'){{ $forum->name }} | {{ $appSettingsForumName }}
@endsection

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading" style="overflow: hidden; line-height:30px;">
            <span id="forum-name-text">{{ $forum->name }}</span>
            @if(Auth::check())
                <a href="{{ action('TopicController@create', $forum->id) }}" class="btn btn-primary btn-sm pull-right">
                    {{ trans('topic.create') }}
                </a>
            @endif
        </div>
        @unless($topics->isEmpty())
            <table class="panel-body table table-hover table-bordered">
                <thead>
                    <tr class="row">
                        <td>{{ trans('topic.topicname') }}</td>
                        <td class="col-xs-2 col-sm-2 col-md-1 col-lg-1 text-center">{{ trans('topic.comments') }}</td>
                        <td class="col-xs-3 col-sm-3 col-md-3 col-lg-2">{{ trans('topic.lastcomment') }}</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($topics as $topic)
                        <tr class="row @if($topic->isAttached()) info @endif ">
                            <td>
                                <a href="{{ url('topic/'.$topic->id) }}">{{ $topic->title }}</a>
                                <br>
                                <small><a href="{{ url('user/'.$topic->user->id) }}">{{$topic->user->name}}</a></small>
                            </td>
                            <td class="col-xs-2 col-sm-2 col-md-1 col-lg-1 text-center">{{ $topic->comments->count() }}</td>
                            <td class="col-xs-3 col-sm-3 col-md-3 col-lg-2">
                                @if($topic->lastComment())
                                    <a href="{{ url('user/'.$topic->lastComment()->user->id) }}">{{ $topic->lastComment()->user->name }}</a>
                                    <br>
                                    <small>{{ LocalizedCarbon::instance($topic->lastComment()->updated_at)->diffForHumans() }}</small>
                                @else
                                    {{ trans('topic.nocomments') }}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <h1 class="text-center">{{ trans('forum.notopics') }}</h1><br>
        @endunless
        <div class="panel-footer" style="overflow: hidden;">
            @role('admin')
                <div class="btn-group pull-left">
                    <button value="{{$forum->id}}" class="btn btn-primary btn-sm open-modal">{{ trans('forum.rename') }}</button>
                    <button value="{{$forum->id}}" class="btn btn-danger btn-sm delete-forum">{{ trans('forum.delete') }}</button>
                </div>
            @endrole
            <style>
                .pagination { display: inline-block; margin: 0; }
            </style>
            <div class="pull-right">{!! $topics->links() !!}</div>
        </div>
    </div>
</div>

<div class="modal fade" id="forumModal" tabindex="-1" role="dialog" aria-labelledby="forumModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="forumModalLabel">{{ trans('forum.rename') }}</h4>
            </div>
            <div class="modal-body">
                <form id="frmForum" name="frmForum" novalidate onsubmit="return false;">
                    <div class="form-group">
                        <input type="text" class="form-control" id="forum-name" name="name" placeholder="{{ trans('forum.modalplaceholder') }}">
                    </div>
                </form>
                <div id="form-errors"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn-save">{{ trans('global.save') }}</button>
                <input type="hidden" id="forum_id" name="forum_id" value="0">
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-scripts')
    <script src="{{ asset('js/forums.js') }}"></script>
@endsection