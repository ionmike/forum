@extends('layouts.app')

@section('title'){{ $appSettingsForumName }}
@endsection

@section('content')
    <div style="padding-top: 10%;">
        <h2 class="text-center">{{ trans('errorpage.405') }}</h2>
    </div>
@endsection