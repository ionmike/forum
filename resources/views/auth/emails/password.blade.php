{{ trans('passwords.reset_label') }}: <a href="{{ $link = LaravelLocalization::localizeURL('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}"> {{ $link }} </a>
