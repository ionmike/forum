<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute должен быть допустим.',
    'active_url'           => ':attribute не является верным URL.',
    'after'                => ':attribute должен быть после :date.',
    'alpha'                => ':attribute может содержать только символы.',
    'alpha_dash'           => ':attribute может содержать только символы, цифры или черточки.',
    'alpha_num'            => ':attribute может содержать только символы и цифры.',
    'array'                => ':attribute должен быть массивом.',
    'before'               => ':attribute должен быть до :date.',
    'between'              => [
        'numeric' => ':attribute должеть быть между :min и :max.',
        'file'    => ':attribute должеть быть между :min и :max килобайт.',
        'string'  => ':attribute должеть быть между :min и :max символов.',
        'array'   => ':attribute должен иметь между :min и :max предметов.',
    ],
    'boolean'              => ':attribute должен быть правдой или ложью.',
    'confirmed'            => ':attribute не совпадает с ранее введенным.',
    'date'                 => ':attribute не является правильной датой.',
    'date_format'          => ':attribute не подходит под формат :format.',
    'different'            => ':attribute и :other должны быть разными.',
    'digits'               => ':attribute должен укладываться в :digits цифр.',
    'digits_between'       => ':attribute должеть быть между :min и :max.',
    'email'                => ':attribute должен быть правильным E-Mail.',
    'exists'               => 'Выбранный :attribute неверен.',
    'filled'               => ':attribute требуется.',
    'image'                => ':attribute не является изображением.',
    'in'                   => 'Выбранный :attribute неверен.',
    'integer'              => ':attribute не число.',
    'ip'                   => ':attribute не верный IP адресс.',
    'json'                 => ':attribute должен являтся правильной JSON строкой.',
    'max'                  => [
        'numeric' => ':attribute не может быть больше :max.',
        'file'    => ':attribute не может быть больше :max килобайт.',
        'string'  => ':attribute не может быть больше :max символов.',
        'array'   => ':attribute не может включать больше :max предметов.',
    ],
    'mimes'                => ':attribute должен быть файлом типа: :values.',
    'min'                  => [
        'numeric' => ':attribute должен быть больше чем :min.',
        'file'    => ':attribute должен быть больше, чем :min килобайт.',
        'string'  => ':attribute должен быть больше, чем :min символов.',
        'array'   => ':attribute должен содержать больше, чем :min предметов.',
    ],
    'not_in'               => 'Выбранный атрибут :attribute неверен.',
    'numeric'              => ':attribute должен являтся числом.',
    'regex'                => ':attribute формат неверен.',
    'required'             => 'Требуется :attribute',
    'required_if'          => ':attribute требуется пока :other = :value.',
    'required_unless'      => ':attribute требуется пока :other в пределе :values.',
    'required_with'        => ':attribute требуется пока :values представлен.',
    'required_with_all'    => ':attribute требуется пока :values представлен.',
    'required_without'     => ':attribute требуется пока :values не представлен.',
    'required_without_all' => ':attribute требуется пока не один из :values представлены.',
    'same'                 => ':attribute и :other должны совпадать.',
    'size'                 => [
        'numeric' => ':attribute должен быть размером :size.',
        'file'    => ':attribute должен быть размером :size килобайт.',
        'string'  => ':attribute должен быть размером в :size символов.',
        'array'   => ':attribute должен включать :size предметов.',
    ],
    'string'               => ':attribute должен быть строкой.',
    'timezone'             => ':attribute должен быть правильной зоной.',
    'unique'               => ':attribute уже занят.',
    'url'                  => 'Формат :attribute не правильный.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'email'         => 'E-Mail',
        'password'      => 'Пароль',
        'name'          => 'имя',
        'title'         => 'заголовок',
        'text'          => 'текст',
        'profile_image' => 'Аватар',
    ],

];
