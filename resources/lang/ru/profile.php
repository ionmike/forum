<?php

return [
    'myprofile'        => 'Мой профиль',
    'registeredat'     => 'Зарегестрировался',
    'lastfivetopics'   => 'Последние 5 созданных тем',
    'lastfivecomments' => 'Последние 5 комментариев',
    'settings'         => 'Настройки',
    'usersettings'     => 'Настройки пользователя',
    'changepassword'   => 'Сменить пароль',
    'roles'            => 'Роли',
    'avatar'           => 'Аватар',
];