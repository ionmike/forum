<?php

return [
    '403' => 'У Вас нет разрешений на выполнение этой операции.',
    '405' => 'Метод не разрешим.',
    '503' => 'Сайт находится на техническом обслуживании.'
];