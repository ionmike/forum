<?php

return [
    'register'   => 'Регистрация',
    'doregister' => 'Зарегистрироваться',
    'name'       => 'Имя',
    'email'      => 'E-Mail',
    'password'   => 'Пароль',
    'confirm'    => 'Повторите пароль',
];