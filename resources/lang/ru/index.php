<?php

return [
    'search'          => 'Поиск',
    'admincp'         => 'Панель администратора',
    'lasttentopics'   => 'Последние 10 обсуждений',
    'lasttencomments' => 'Последние 10 комментариев',
    'topicnums'       => '{0} Тем нет| тема|[2,4] темы|[5,Inf] тем',
];