<?php

return [
    'login'         => 'Login',
    'logout'        => 'Logout',
    'forgot'        => 'Forgot Your Password?',
    'remember'      => 'Remember Me',
    'email_address' => 'E-Mail Address',
    'password'      => 'Password',
    'failed'        => 'These credentials do not match our records.',
    'throttle'      => 'Too many login attempts. Please try again in :seconds seconds.',
];