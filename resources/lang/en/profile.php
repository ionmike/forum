<?php

return [
    'myprofile'        => 'My profile',
    'registeredat'     => 'Registered',
    'lastfivetopics'   => 'Last 5 topics',
    'lastfivecomments' => 'Last 5 comments',
    'settings'         => 'Settings',
    'usersettings'     => 'User Settings',
    'changepassword'   => 'Change password',
    'roles'            => 'Roles',
    'avatar'           => 'User Picture',
];