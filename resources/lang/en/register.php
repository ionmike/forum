<?php

return [
    'register'   => 'Register',
    'doregister' => 'Register',
    'name'       => 'Name',
    'email'      => 'E-Mail Address',
    'password'   => 'Password',
    'confirm'    => 'Confirm Password',
];