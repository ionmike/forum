<?php

return [
    'create'           => 'Create group',
    'rename'           => 'Rename group',
    'delete'           => 'Delete group',
    'created'          => 'You have created forum group!',
    'updated'          => 'Forum group name updated!',
    'deleted'          => 'Forum group deleted!',
    'editmodal'        => 'Edit group name',
    'addmodal'         => 'Add forum group',
    'modalplaceholder' => 'Forum group name',
];