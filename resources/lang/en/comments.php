<?php

return [
    'create'           => 'Write comment',
    'edit'             => 'Edit comment',
    'delete'           => 'Delete comment',
    'writeplaceholder' => 'Write your comment here',
    'modalplaceholder' => 'Comment text',
    'editor'           => 'Comment editor',
    'nocomments'       => 'There are no comments at that page!',
    'updated'          => 'Comment updated',
    'deleted'          => 'Comment deleted!',
    'changed_at'       => 'Changed at',
];