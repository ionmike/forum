<?php

return [
    'create'           => 'Add forum',
    'rename'           => 'Rename forum',
    'delete'           => 'Delete this forum',
    'notopics'         => 'There are no topics!',
    'deleted'          => 'You have deleted forum!',
    'modalplaceholder' => 'Forum name',
    'created'          => 'Forum created!',
    'updated'          => 'Forum name updated!',
];