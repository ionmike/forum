<?php

return [
    'sure'   => 'Are you sure?',
    'save'   => 'Save changes',
    'create' => 'Create',
];