<?php

return [
    'topic'     => 'Topic',
    'user'      => 'User',
    'comment'   => 'Comment',
    'message'   => 'Start search by typing keyword and select what you want to find.',
    'noresults' => 'Nothing found',
];