<?php

return [
    '403' => 'Seems like you have no permissions to do that.',
    '405' => 'Method not allowed.',
    '503' => 'Site is on maintenance mode.'
];