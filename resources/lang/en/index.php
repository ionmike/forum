<?php

return [
    'search'          => 'Search',
    'admincp'         => 'Toggle admin CP',
    'lasttentopics'   => 'Last 10 topics',
    'lasttencomments' => 'Last 10 comments',
    'topicnums'       => '{0} No topics| topic|[2,Inf] topics',
];