<?php

return [
    'create'      => 'Create topic',
    'edit'        => 'Edit topic',
    'delete'      => 'Delete topic',
    'comments'    => 'Comments',
    'nocomments'  => 'No comments',
    'lastcomment' => 'Last comment',
    'attached'    => 'Attached',
    'topicname'   => 'Topic name',
    'title'       => 'Topic title',
    'backtoforum' => 'Back to forum',
    'created'     => 'You have created a topic!',
    'updated'     => 'Topic updated',
    'deteted'     => 'You have deleted topic!',
];