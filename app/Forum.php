<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'forum_group_id'];

    /**
     * Forum included in forum group.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function forumGroup()
    {
        return $this->belongsTo(ForumGroup::class);
    }

    /**
     * Forum have topics.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function topics()
    {
        return $this->hasMany(Topic::class);
    }

    /**
     * Get last topic in forum.
     *
     * @return mixed
     */
    public function lastActiveTopic()
    {
        return $this->topics()->lastActivity()->first();
    }
}
