<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Topic extends Model
{
    use Eloquence;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'text', 'forum_id', 'attached'];

    /**
     * The attributes that used for search.
     *
     * @var array
     */
    protected $searchableColumns = ['title', 'text'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['last_activity'];

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLastActivity($query)
    {
        return $query->latest('last_activity');
    }

    // public function scopeGetAttached()

    /**
     * Topic belong to forum.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function forum()
    {
        return $this->belongsTo(Forum::class);
    }

    /**
     * Topic have lost of comments.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * Topic have user that create topic.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get last comment in topic.
     *
     * @return mixed
     */
    public function lastComment()
    {
        return $this->comments->last();
    }

    /**
     * If topic attached.
     *
     * @return bool
     */
    public function isAttached()
    {
        return $this->attached == 1 ? true : false;
    }

    /**
     * Get Reply Page
     *
     * Returns the page number where a reply resides as it relates to pagination
     *
     * @param null $commentId Optional ID for specific reply
     * @param bool $pageLink If True, return a GET parameter ?page=x
     * @param int $paginate Number of posts per page
     * @return int|null|string // Int for only the page number, null if no replies, String if $pageLink == true
     */
    public function getCommentPage($commentId = null, $pageLink = true, $paginate = 15)
    {
        // Find the page for a specific reply if provided, otherwise find the most
        // recent post's ID. If there are no replies, return null and exit.
        if (!$commentId) {
            $lastComment = $this->lastComment();
            if ($lastComment) {
                $commentId = $lastComment->id;
            } else {
                return null;
            }
        }

        // Find the number of replies to this Topic prior to the one in question
        $count = $this->comments()->where('id', '<', $commentId)->count();

        $page = (int)($count / $paginate + 1);

        // Return either the integer or URL parameter
        return $pageLink ? "?page=$page" : $page;
    }

}
