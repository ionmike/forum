<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Comment extends Model
{
    use Eloquence;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['text', 'topic_id'];

    /**
     * The attributes that used for search.
     *
     * @var array
     */
    protected $searchableColumns = ['text'];

    /**
     * Comment related to topic.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function topic()
    {
        return $this->belongsTo(Topic::class);
    }

    /**
     * Comment have user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Check if comment was changed by time.
     *
     * @return bool
     */
    public function isEdited()
    {
        if ($this->created_at != $this->updated_at) {
            return true;
        } else {
            return false;
        }
    }
}
