<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Bican\Roles\Traits\HasRoleAndPermission;
use Bican\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;
use Sofa\Eloquence\Eloquence;

class User extends Authenticatable implements HasRoleAndPermissionContract
{
    use HasRoleAndPermission, Eloquence;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes that used for search.
     *
     * @var array
     */
    protected $searchableColumns = ['name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Get IDs of roles attached to user.
     *
     * @return array
     */
    public function getRolesListAttribute()
    {
        return $this->getRoles()->lists('id')->all();
    }

    /**
     * Update roles for user.
     *
     * @param array $roles
     * @return array|int
     */
    public function syncRoles($roles = array())
    {
        $this->roles = null;

        return empty($roles) ? $this->detachAllRoles() : $this->roles()->sync($roles);
    }

    /**
     * User have comments in topic.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * User have topics.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function topics()
    {
        return $this->hasMany(Topic::class);
    }
}
