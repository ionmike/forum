<?php

namespace App\Http\Controllers;

use App\Topic;
use App\Comment;
use App\ForumGroup;

class IndexController extends Controller
{
    /**
     * Show all forums at index page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $forumGroups = ForumGroup::all();

        $lastTenTopics = Topic::latest()->take(10)->get();
        $lastTenComments = Comment::latest('updated_at')->take(10)->get();

        return view('index', compact('forumGroups', 'lastTenTopics', 'lastTenComments'));
    }
}
