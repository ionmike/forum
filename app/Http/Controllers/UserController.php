<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Topic;
use App\Comment;
use Bican\Roles\Models\Role;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{
    /**
     * Only auth user can see/edit profile pages.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($id)
    {
        $user = User::findOrFail($id);

        $userLastFiveTopics = Topic::take(5)->where('user_id', '=', $user->id)->latest()->get();
        $userLastFiveComments = Comment::take(5)->where('user_id', '=', $user->id)->latest('updated_at')->get();

        return view('user.profile', compact('user', 'userLastFiveTopics', 'userLastFiveComments'));
    }

    /**
     * Show page for user edit.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        $roles = Role::lists('name', 'id');

        if(Auth::user()->id == $id || Auth::user()->hasRole('admin')) {
            return view('user.edit', compact('user', 'roles'));
        } else {
            return abort(403);
        }
    }

    /**
     * Store updated user in repository.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:25',
            'email' => 'required|email|max:255',
            'password' => 'confirmed|min:6',
            'profile_image' => 'mimes:jpeg,bmp,png|max:500'
        ]);

        $user = User::findOrFail($id);

        if($user->id != $id) {
            return abort(403);
        }

        if(Auth::user()->id == $id || Auth::user()->hasRole('admin')) {
            $user->update(
                [
                    'name' => $request->name,
                    'email' => $request->email
                ]
            );

            if (!empty($request->password)) {
                $user->password = bcrypt($request->password);
                $user->save();
            }

            if (Auth::user()->hasRole('admin')) {
                $user->syncRoles($request->roles_list);
            }

            if($request->hasFile('profile_image')) {
                $path = '/images/uploads/user_avatars/';
                if ($user->avatar != 'default_avatar.jpg') {
                    $old_image = $user->avatar;
                    unlink(sprintf(public_path() . $path . '%s', $old_image));
                }

                $file = $request->file('profile_image');
                $image_name = time() . '-' . $file->getClientOriginalName();
                $file->move(public_path() . $path, $image_name);

                Image::make(sprintf(public_path() . $path . '%s', $image_name))->resize(100, 100)->save();

                $user->avatar = $image_name;
            }

            $user->save();

            return redirect('user/' . $user->id);

        } else {
            return abort(403);
        }
    }
}
