<?php

namespace App\Http\Controllers;

use App\ForumGroup;
use Illuminate\Http\Request;
use Yuansir\Toastr\Facades\Toastr;

class ForumGroupController extends Controller
{
    /**
     * Only auth users can do actions with CRUD to forum groups.
     */
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    /**
     * Store a newly created forum group in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $forumGroup = ForumGroup::create($request->all());

        Toastr::success(trans('forumgroup.created'));

        return response()->json($forumGroup);
    }

    /**
     * Show the form for editing the specified forum group.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $forumGroup = ForumGroup::findOrFail($id);

        return response()->json($forumGroup);
    }

    /**
     * Update forum group.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $forumGroup = ForumGroup::findOrFail($id);
        $forumGroup->name = $request->name;
        $forumGroup->save();

        return response()->json($forumGroup);
    }

    /**
     * Remove forum group and delete all items inside it.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $forumGroup = ForumGroup::destroy($id);

        Toastr::warning(trans('forumgroup.deleted'));

        return response()->json($forumGroup);
    }
}
