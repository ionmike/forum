<?php

namespace App\Http\Controllers;

use App\Forum;
use Illuminate\Http\Request;
use Yuansir\Toastr\Facades\Toastr;

class ForumController extends Controller
{
    /**
     * Only auth users can do actions with CRUD in forums.
     */
    public function __construct()
    {
        $this->middleware('role:admin', ['except'=>'show']);
    }

    /**
     * Create new forum with group id from request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $forum = Forum::create($request->all());

        return response()->json($forum);
    }

    /**
     * Display the specified forum.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $forum = Forum::find($id);
        $topics = $forum->topics()->orderBy('attached', 'desc')->lastActivity()->paginate(20);

        return view('forum', compact('forum', 'topics'));
    }

    /**
     * Get forum data in json for ajax request.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        $forum = Forum::findOrFail($id);
        return response()->json($forum);
    }

    /**
     * Update the specified forum in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $forum = Forum::findOrFail($id);
        $forum->name = $request->name;
        $forum->save();

        return response()->json($forum);
    }

    /**
     * Remove the specified forum and all related items.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $forum = Forum::destroy($id);

        Toastr::warning(trans('forum.deleted'));

        return response()->json($forum);
    }
}
