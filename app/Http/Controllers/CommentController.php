<?php

namespace App\Http\Controllers;

use Auth;
use App\Topic;
use App\Comment;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    private $user;

    /**
     * Only auth users can do actions with CRUD comments.
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->user = Auth::user();
    }

    /**
     * Store a newly created message in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'text' => 'required|min:5|max:2048',
        ]);

        $comment = new Comment($request->all());

        $topic = Topic::find($request->topic_id);
        $topic->last_activity = Carbon::now();

        $this->user->comments()->save($comment);
        $topic->save();

        return redirect('/topic/'.$request->topic_id);
    }

    /**
     * Get comment data in json for ajax request.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        $comment = Comment::findOrFail($id);

        if($comment->user_id == $this->user->id || $this->user->hasRole('admin')) {
            return response()->json($comment);
        } else {
            return response()->json(['Forbidden'], 403);
        }
    }

    /**
     * Validate and update comment.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'text' => 'required|min:5|max:2048',
        ]);

        $comment = Comment::findOrFail($id);

        if($comment->user_id == $this->user->id || $this->user->hasRole('admin')) {
            $comment->text = $request->text;
            $comment->save();

            return response()->json($comment);
        } else {
            return response()->json(['Forbidden'], 403);
        }
    }

    /**
     * Remove the specified comment from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::findOrFail($id);

        if($comment->user_id == $this->user->id || $this->user->hasRole('admin')) {
            $comment->delete();
            return response()->json($comment);
        } else {
            return response()->json(['Forbidden'], 403);
        }
    }
}