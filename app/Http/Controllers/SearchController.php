<?php

namespace App\Http\Controllers;

use App\User;
use App\Topic;
use App\Comment;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
     * Only auth users can access search.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Search by type and keyword on forum.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $searchText = $request->search_text;
        $searchType = $request->type;
        if($searchText == '') {
            return view('search.index');
        }

        switch($searchType) {
            case 'T':
                $results = Topic::search($searchText)->paginate(20);
                break;
            case 'C':
                $results = Comment::search($searchText)->paginate(20);
                break;
            case 'U':
                $results = User::search($searchText)->paginate(20);
                break;
        }

        return view('search.index', compact('results', 'searchText', 'searchType'));
    }
}
