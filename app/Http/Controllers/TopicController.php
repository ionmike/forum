<?php

namespace App\Http\Controllers;

use Auth;
use App\Topic;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yuansir\Toastr\Facades\Toastr;

class TopicController extends Controller
{
    private $user;
    /**
     * Instantiate a new TopicController instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth', ['except'=>['show']]);
        $this->user = Auth::user();
    }

    /**
     * Display the specified topic.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $topic = Topic::findOrFail($id);
        $comments = $topic->comments()->paginate(15);

        return view('topics.index', compact('topic', 'comments'));
    }

    /**
     * Show the form for creating a new topic.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('topics.create', ['forum_id'=> $id]);
    }

    /**
     * Store a newly created topic in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'    => 'required|min:5|max:75',
            'text'     => 'required|min:10|max:20000',
            'attached' => 'boolean',
        ]);

        if($this->user->hasRole('admin')) {
            $request['attached'] ?: $request['attached'] = 0;
        }

        $topic = new Topic($request->all());
        $topic->last_activity = Carbon::now();
        $this->user->topics()->save($topic);

        Toastr::success(trans('topic.created'));

        return redirect('/topic/'.$topic->id);
    }

    /**
     * Show the form for editing the specified topic.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $topic = Topic::findOrFail($id);
        if($topic->user_id == $this->user->id || $this->user->hasRole('admin')) {
            return view('topics.edit', ['topic' => $topic]);
        } else {
            return abort(403);
        }
    }

    /**
     * Update the specified topic in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'    => 'required|min:5|max:75',
            'text'     => 'required|min:10|max:20000',
            'attached' => 'boolean',
        ]);

        if($this->user->hasRole('admin')) {
            $request['attached'] ?: $request['attached'] = 0;
        }

        $topic = Topic::findOrFail($id);
        if($topic->user_id == $this->user->id || $this->user->hasRole('admin'))  {
            $topic->update($request->all());
            $topic->last_activity = Carbon::now();
            $topic->save();

            Toastr::info(trans('topic.updated'));

            return redirect('/topic/' . $topic->id);
        } else {
            return abort(403);
        }
    }

    /**
     * Remove the specified topic and related comments from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $topic = Topic::findOrFail($id);
        if($topic->user_id == $this->user->id || $this->user->hasRole('admin')) {
            $topic->delete();

            Toastr::warning(trans('topic.deteted'));

            return redirect('forum/' . $topic->forum_id);
        } else {
            return abort(403);
        }
    }
}
