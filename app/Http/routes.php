<?php
Route::group(['middleware' => 'web'], function() {

    Route::group(['prefix' => 'ajax'], function() {
        Route::post('forum/group', 'ForumGroupController@store');
        Route::patch('forum/group/{id}/update', 'ForumGroupController@update');
        Route::delete('forum/group/{id}/destroy', 'ForumGroupController@destroy');
        Route::get('forum/group/{id}/edit', 'ForumGroupController@edit');

        Route::post('forum', 'ForumController@store');
        Route::get('forum/{id}/edit', 'ForumController@edit');
        Route::patch('forum/{id}/update', 'ForumController@update');
        Route::delete('forum/{id}/destroy', 'ForumController@destroy');

        Route::get('comment/{id}/edit', 'CommentController@edit');
        Route::patch('comment/{id}/update', 'CommentController@update');
        Route::delete('comment/{id}/destroy', 'CommentController@destroy');
    });

    Route::group(['middleware' => ['localeSessionRedirect', 'localizationRedirect'], 'prefix' => LaravelLocalization::setLocale()], function () {
        Route::auth();

        Route::get('/', 'IndexController@index');

        Route::get('forum/{id}', 'ForumController@show');

        Route::get('topic/{id}', 'TopicController@show');
        Route::get('topic/create/{id}', 'TopicController@create');
        Route::post('topic/create', 'TopicController@store');
        Route::get('topic/{id}/edit', 'TopicController@edit');
        Route::patch('topic/{id}', 'TopicController@update');

        Route::get('topic/{id}/destroy', 'TopicController@destroy');

        Route::post('comment/create', 'CommentController@store');

        Route::get('user/{id}', 'UserController@show');
        Route::get('user/{id}/edit', 'UserController@edit');
        Route::patch('user/{id}', 'UserController@store');

        Route::get('search', 'SearchController@search');
    });
});