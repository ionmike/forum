# Forum creator

Create your own forum from the void without any pain.
'No admin panel' project.
> My university course project. 
> Built on Laravel 5.2

### Installation
Rename .env.example to .env and config it.
```sh
$ composer install
$ php artisan migrate
$ php artisan key:generate
$ php artisan js-localization:refresh
```

Optional
```sh
$ php artisan db:seed (if ypu want to seed dummy data)
```

Default admin credentials: 
> E-Mail: admin@forum.com 
> Password: admin